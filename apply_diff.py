#!/usr/bin/env python3.10

import re
import sys
from dataclasses import dataclass
from enum import Enum
from typing import Optional

CHANGE_PATTERN = re.compile(
    r"(?P<op>[ +-])(?P<name>\w+) (?P<current_state>\w+)( -> (?P<new_state>\w+))?",
    re.MULTILINE,
)
OPERATIONS = {" ": "modify", "+": "add", "-": "remove"}
CONFIG_NOT_SET_PATTERN = re.compile(r"# CONFIG_(?P<name>\w+) is not set")
CONFIG_PATTERN = re.compile(r"CONFIG_(?P<name>\w+)=(?P<state>\S+)")


class Operation(Enum):
    MODIFY = " "
    ADD = "+"
    REMOVE = "-"


class StateType(Enum):
    YES = "y"
    NO = "n"
    MODULE = "m"
    NOTSET = "notset"
    OTHER = "other"


@dataclass(frozen=True)
class Change:
    operation: Operation
    name: str
    current_state: Option  # noqa
    new_state: Option  # noqa

    @staticmethod
    def from_str(config_line: str) -> Change:  # noqa
        if match := CHANGE_PATTERN.fullmatch(
            config_line.rstrip(),
        ):
            name = match["name"]
            new_state = match["new_state"]
            return Change(
                Operation(match["op"]),
                name,
                Option.from_state(name, match["current_state"]),
                new_state and Option.from_state(name, match["new_state"]),
            )
        raise Exception(f"Error parsing line: {config_line}")


@dataclass(frozen=True)
class Option:
    name: str
    state: StateType
    other_value: str = None

    @staticmethod
    def from_state(name: str, state: str) -> Option:  # noqa
        try:
            return Option(name, StateType(state))
        except ValueError:
            return Option(name, StateType.OTHER, state)

    @staticmethod
    def from_config_line(line: str) -> Optional[Option]:  # noqa
        if match := CONFIG_NOT_SET_PATTERN.match(line.strip()):
            return Option(match["name"], StateType.NOTSET)

        if match := CONFIG_PATTERN.fullmatch(line.strip()):
            try:
                # y, n or m
                state = StateType(match["state"])
            except ValueError:
                state = StateType.OTHER
                state.other_value = match["state"]

            return Option(match["name"], state)

    def to_config_line(self):
        return f"CONFIG_{self.name}={self.other_value or self.state.value}"


def main() -> None:
    changes: dict[str, Change] = {}
    additions: set[Change] = set()
    for line in sys.stdin.readlines():
        change = Change.from_str(line)
        changes[change.name] = change

        if change.operation == Operation.ADD:
            additions.add(change)

    for line in open(sys.argv[1]):
        if option := Option.from_config_line(line):
            if change := changes.get(option.name):
                if change.operation == Operation.MODIFY:
                    print(change.new_state.to_config_line())
                    continue
                if change.operation == Operation.REMOVE:
                    continue  # Option gets removed
            else:
                print(line, end="")  # No change
        else:  # Not a parseable option, just print it
            print(line, end="")

    for addition in additions:
        print(addition.new_state.to_config_line(), end="")


if __name__ == "__main__":
    main()
