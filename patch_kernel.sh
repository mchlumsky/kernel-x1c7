#!/usr/bin/env bash

KERNEL_X1C7_RELEASE=1
git checkout f34
git pull
cp -f /kernel-x86_64-fedora.config /kernel
sed -i "s/# define buildid .local/%define buildid .x1c7.$KERNEL_X1C7_RELEASE/g" kernel.spec
sed -i 's/%define debugbuildsenabled 0/%define debugbuildsenabled 1/g' kernel.spec
sed -i 's/%global released_kernel 0/%global released_kernel 1/g' kernel.spec
dnf builddep -y kernel.spec
fedpkg prep
