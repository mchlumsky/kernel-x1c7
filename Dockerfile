FROM fedora:34
RUN dnf install -y vim-enhanced fedpkg fedora-packager rpmdevtools ncurses-devel pesign grubby copr-cli && dnf clean all
RUN fedpkg clone -a kernel
RUN mkdir /root/.config
COPY kernel-x86_64-fedora.config /
WORKDIR /kernel
COPY patch_kernel.sh .
ENTRYPOINT ["bash"]
