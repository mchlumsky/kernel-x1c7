# Custom kernel for Thinkpad X1 Carbon Gen 7

## Build container for kernel RPMs creation

    podman build --tag kernel-x1c7:latest .

## Build RPMs

    podman run --rm -it kernel-x1c7:latest

    # Inside the container
    ./patch_kernel.sh

    # Test by building locally
    fedpkg local

    # When ready to push to copr (you'll need an API token in /root/.config/copr)
    fedpkg srpm
    copr-cli build --timeout 43200 kernel-x1c7 kernel-<version>.src.rpm
